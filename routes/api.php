<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


//Both controllers

Route::post('/both/create','Api\BothController@insetBoth');
Route::post('/both/all','Api\BothController@getBoth');
Route::post('/both/single','Api\BothController@getBothById');




