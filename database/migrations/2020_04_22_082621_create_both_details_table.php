<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBothDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('both_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('slider_images');
            $table->bigInteger('both_id');

            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::dropIfExists('both_details');
    }
}
