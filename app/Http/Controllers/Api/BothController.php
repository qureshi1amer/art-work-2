<?php

namespace App\Http\Controllers\Api;

use App\Both;
use App\BothDetails;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;

class BothController extends Controller
{
    public $auth_token = "artcart_12";
    public $response_array = [];

    //Both methods

    public function insetBoth(Request $request)
    {
        if (!empty($request->auth_token) && $request->auth_token == $this->auth_token) {

            $rules = [
                'name' => 'required|string|max:255',
                'title' => 'required|string|max:255',
                'title_image' => 'mimes:jpeg,jpg,png|required|max:1000',
                'description' => 'required'];

            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                $this->response_array = [
                    'status' => '400',
                    'message' => $validator->errors()];
            } else {
                $imageName = time() . '.' . $request->title_image->extension();
                $request->title_image->move(public_path('images'), $imageName);
                $request->title_image = $imageName;
                Both::create($this->makePattern($request, $imageName));

                $this->response_array = [
                    'status' => '200',
                    'message' => 'Both created successfully '];
            }
        } else {
            $this->response_array = [
                'status' => '401',
                'message' => 'Invalid Auth Token'];
        }

        return json_encode($this->response_array);

    }

    public function getBoth(Request $request)
    {
        if (!empty($request->auth_token) && $request->auth_token == $this->auth_token) {

            $both = Both::paginate(20);

            $this->response_array = [
                'status' => '200',
                'message' => 'success',
                'response' => $both];
        } else {
            $this->response_array = [
                'status' => '401',
                'message' => 'Invalid Auth  Token'];
        }
        return json_encode($this->response_array);

    }

    public function getBothById(Request $request)
    {
        if (!empty($request->auth_token) && $request->auth_token == $this->auth_token) {
            $both = Both::find($request->id);
            if ($both) {
                $images = BothDetails::where('both_id', $request->id)->get();
                $both['slider_images'] = $images;
                $this->response_array = [
                    'status' => '200',
                    'message' => 'success',
                    'response' => $both];
            } else {
                $this->response_array = [
                    'status' => '400',
                    'message' => 'no both found'];

            }
        } else {
            $this->response_array = [
                'status' => '401',
                'message' => 'Invalid Auth  Token'];
        }
        return json_encode($this->response_array);
    }
    public function deleteBoth(Request $request)
    {
        if (!empty($request->auth_token) && $request->auth_token == $this->auth_token) {
            $both = Both::find($request->id);
            if ($both) {
                Both::where('id', $request->id)->delete();
                $this->response_array = [
                    'status' => '200',
                    'message' => 'deleted successfully'];
            } else {
                $this->response_array = [
                    'status' => '400',
                    'message' => 'no both found'];
            }
        } else {
            $this->response_array = [
                'status' => '401',
                'message' => 'Invalid Auth  Token'];
        }
        return json_encode($this->response_array);
    }

    public function makePattern(Request $request, $name)
    {
        $pattern = [
            'name' => $request->name,
            'title' => $request->title,
            'title_image' => $name,
            'description' => $request->description
        ];
        return $pattern;
    }
}
